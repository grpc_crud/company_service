package postgres

import (
	"fmt"

	"bitbucket.org/Udevs/company_service/genproto/company_service"
	"bitbucket.org/Udevs/company_service/storage/repo"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
)

type companyRepo struct {
	db *sqlx.DB
}

func NewCompanyRepo(db *sqlx.DB) repo.CompanyRepoI {
	return &companyRepo{db: db}
}

func (r *companyRepo) Create(req *company_service.CreateCompany) (string, error) {
	var (
		id uuid.UUID
	)

	tx, err := r.db.Begin()

	if err != nil {
		return "", err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	id, err = uuid.NewRandom()
	if err != nil {
		return "", nil
	}
	query := ` INSERT INTO company (id,name) VALUES($1, $2) `

	_, err = tx.Exec(query, id, req.Name)

	if err != nil {
		return "", nil
	}

	return id.String(), nil
}

func (r *companyRepo) Get(id string) (*company_service.Company, error) {
	var company company_service.Company
	query := ` SELECT id, name FROM company WHERE id =$1 `

	row := r.db.QueryRow(query, id)
	err := row.Scan(
		&company.Id,
		&company.Name,
	)
	if err != nil {
		return nil, err
	}

	return &company, nil
}

func (r *companyRepo) GetAll(req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error) {
	var (
		args     = make(map[string]interface{})
		filter   string
		companys []*company_service.Company
		count    uint32
	)

	if req.Name != "" {
		filter += ` AND name ilike '%' || :name || '%' `
		args["name"] = req.Name
	}

	countQuery := `SELECT count(1) FROM company WHERE true ` + filter
	rows, err := r.db.NamedQuery(countQuery, args)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		err = rows.Scan(
			&count,
		)

		if err != nil {
			return nil, err
		}
	}

	filter += " OFFSET :offset LIMIT :limit "
	args["limit"] = req.Limit
	args["offset"] = req.Offset

	query := `
		SELECT 
			id, 
			name
		FROM 
			company WHERE true ` + filter

	rows, err = r.db.NamedQuery(query, args)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		var company company_service.Company

		err = rows.Scan(
			&company.Id,
			&company.Name,
		)

		if err != nil {
			return nil, err
		}

		companys = append(companys, &company)
	}

	if err := rows.Close(); err != nil {
		return nil, err
	}

	return &company_service.GetAllCompanyResponse{
		Company: companys,
		Count:   count,
	}, nil
}

func (r *companyRepo) Update(req *company_service.Company) (*company_service.Company, error) {
	query := `UPDATE company SET
	           name = $1 
				 WHERE id = $2;`

	_, err := r.db.Exec(
		query,
		req.Name,
		req.Id,
	)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}

	return &company_service.Company{
		Id:   req.Id,
		Name: req.Name,
	}, err
}

func (r *companyRepo) Delete(id string) error {
	query := `DELETE FROM company where id = $1`

	_, err := r.db.Exec(query, id)
	return err
}
