package storage

import (
	"bitbucket.org/Udevs/company_service/storage/postgres"
	"bitbucket.org/Udevs/company_service/storage/repo"
	"github.com/jmoiron/sqlx"
)

type StrogeI interface {
	Company() repo.CompanyRepoI
}

type storagePG struct {
	company repo.CompanyRepoI
}

func NewStoragePG(db *sqlx.DB) StrogeI {
	return &storagePG{
		company: postgres.NewCompanyRepo(db),
	}
}

func (s *storagePG) Company() repo.CompanyRepoI {
	return s.company
}
