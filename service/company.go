package service

import (
	"context"

	"bitbucket.org/Udevs/company_service/genproto/company_service"
	"bitbucket.org/Udevs/company_service/pkg/helper"
	"bitbucket.org/Udevs/company_service/pkg/logger"
	"bitbucket.org/Udevs/company_service/storage"
	"github.com/jmoiron/sqlx"
	"google.golang.org/grpc/codes"
	"google.golang.org/protobuf/types/known/emptypb"
)



type companyService struct {
	logger  logger.Logger
	storage storage.StrogeI
}

func NewCompanyService(log logger.Logger, db *sqlx.DB) *companyService {
	return &companyService{
		logger:  log,
		storage: storage.NewStoragePG(db),
	}
}

func (s *companyService) Create(ctx context.Context, req *company_service.CreateCompany) (*company_service.CompanyId, error) {
	id, err := s.storage.Company().Create(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while company", req, codes.Internal)
	}

	return &company_service.CompanyId{
		Id: id,
	}, nil
}

func (s *companyService) Get(ctx context.Context, req *company_service.CompanyId) (*company_service.Company, error) {
	company, err := s.storage.Company().Get(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting company ", req, codes.Internal)
	}

	return company, nil
}

func (s *companyService) GetAll(ctx context.Context, req *company_service.GetAllCompanyRequest) (*company_service.GetAllCompanyResponse, error) {
	professions, err := s.storage.Company().GetAll(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while getting all company ", req, codes.Internal)
	}

	return professions, nil
}

func (s *companyService) Update(ctx context.Context, req *company_service.Company) (*company_service.Company, error) {
	profession, err := s.storage.Company().Update(req)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while  updating company ", req, codes.Internal)
	}

	return profession, nil
}

func (s *companyService) Delete(ctx context.Context, req *company_service.CompanyId) (*emptypb.Empty, error) {
	err := s.storage.Company().Delete(req.Id)
	if err != nil {
		return nil, helper.HandleError(s.logger, err, "error while deleting company ", req, codes.Internal)
	}

	return &emptypb.Empty{}, nil
}